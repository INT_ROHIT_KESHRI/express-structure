module.exports={
   asyncForEach     : require('async-foreach').forEach,
   multer           : require('multer'),
   mkdirp           : require('mkdirp'),
   jwt              : require('jsonwebtoken'),
   crypto           : require('crypto')
}
