var database          = require('../../../../config/database');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var model = {
  getUser :function(condiction,errorcallback,successcallback){
    return qb.select('*')
      .where(condiction)
      .get('users', function(err,response) {
          if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
          successcallback(response);
      }
    );
  },
  updateUser:function(data,condiction,errorcallback,successcallback){
    return qb.update('users', data, condiction, function(err, res) {
      if(err) errorcallback(err);
      successcallback(true);
    });
  }
}
module.exports=model;
