var express             = require('express');
var router              = express.Router();
var app                 = express();
var customodule         = require('../../../customexport');
var node_module         = require('../../../export');
var ERROR               = require('../../../../config/statuscode');
var Authmodel           = require('../model/auth');

router.post('/emailVarify',customodule.validateapi,function(req,res,next){
  var condiction={
    email:req.body.email
  };
  Authmodel.getUser(condiction,function(err){console.log("Error",err);},function(user){
    if(user.length>0){
      var generatedotp=Math.floor((Math.random() * 10000000) + 1);
      var response={
          success     : true,
          resCode     : 200,
          resMessage  : generatedotp
      };
      var userObj = {
          otp: generatedotp
      };
      var cond = {
          id: user[0]['id']
      };
      Authmodel.updateUser(userObj,cond,function(err){},function(success){
        if(success){
          customodule.mail(req.body.email,generatedotp);
          res.status(200).send(response)
        }
      });
    }else{
      res.status(200).send(ERROR.INVALID);
    }
  });
});
module.exports = router;
