module.exports={
   mail             : require('../helper/sendmail'),
   sendnotification : require('../helper/sendnotification'),
   authenticate     : require('../middleware/authenticate'),
   validateapi      : require('../middleware/validateapi'),
   setting          : require('../config/setting')
}
