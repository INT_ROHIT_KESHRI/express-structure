var express         = require('express');
var app             = express();
var port            = 5000;
var bodyParser      = require('body-parser');
/** Backend Api Controllers **/
var Auth            = require('./app/backendapi/auth/controller/auth');
/** End **/
/* App configuration Settings */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended : true,
  limit : '50mb'
}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/* Set the path for all controllers */
app.use('/api/auth', Auth);
/** End **/

app.listen(port , function() {
  console.log('Server started at port ' + port);
});
