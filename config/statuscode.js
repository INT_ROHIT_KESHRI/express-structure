var ERROR={
        BAD_REQ:{
          "success" : false,
          "Statuscode":400,
          "Statustext":"Bad Request"
        },
        UNAUTH:{
          "success" : false,
          "Statuscode":401,
          "Statustext":"Unauthorized"
        },
        INVALID:{
          "success" : false,
          "Statuscode":402,
          "Statustext":"Invalid Email Address"
        },
        INVALIDCREDENTIAL:{
          "success" : false,
          "Statuscode":403,
          "Statustext":"Invalid Credentials"
        }
};
module.exports=ERROR;
